# dsPIC-codes
Codes for the IST/MEFT/Microcontrollers course

## Using stimulus in the simulator
Select the "register injection" tab inside the "stimulus" tab.

Add a new line according to the screenshot bellow. "Data Filename" should be the path of the stimulus file.

An example stimulus file is provided in [stimulus.txt](https://gitlab.com/bernardocarvalho/dsPIC-codes/-/blob/master/Lab2/stimulus.txt)

![](Capture.PNG)

remember to check the green play button on the top left corner for the stimulus to be applied
