/*
Author: J. Oliveira
Edit: D. Hachmeister
*/
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NCOMS 1

#define BAUD 9600

#define UBRRVAL F_CPU/8/BAUD-1

#define TMR_F 5UL
#define PS_VAL 1024UL
#define OSC1A_VAL F_CPU/PS_VAL/TMR_F

#define RX_BUFFER_SIZE 32
#define RXB_MASK 0x1f

char rx_buffer[RX_BUFFER_SIZE];

uint8_t rx_pointer;
uint8_t ind;
uint8_t received = 0;

void uputc(char c);
void uputs(const char *c);
void up_8bits(uint8_t num);

void clear_buffer()
{
	static uint8_t i;
	for (i = 0; i < RX_BUFFER_SIZE; i++)
		rx_buffer[i] = 0;
	rx_pointer = 0;
}

void uputc(char c)
{
	if (c == '\n')
		uputc('\r');
	loop_until_bit_is_set(UCSR0A, UDRE0);	/* wait until we can send a new byte */
	UDR0 = (uint8_t) c;
}

void uputs(const char *c)
{
	do {
		uputc(*c++);
	} while (*c != 0);
}

void up_8bits(uint8_t num) // UART print uint8 as binary string
{
	uint8_t i = 8;
	uputs("0xb");
	do {
		uputc((num & 0x80) ? '1' : '0');
		num = (uint8_t) (num << 1);
	} while (--i > 0);
	uputc('\n');
}

/* timer interrupt */
ISR(TIMER1_COMPA_vect)
{
	PORTB ^= _BV(PB5);			  /* toggle led on pin 13 (PORTB5) */
	// up_8bits(ind);
	ind++;
}

//uart receive isr
ISR(USART_RX_vect)
{
	uint8_t c = UDR0;

	rx_buffer[rx_pointer++] = (char) c;	

	if (c == '!')
		received = 1;
}

int main(void)
{
	uint8_t i;
	/* pin config */
	DDRB = (1 << DDB1);

	/* uart config */
	UCSR0A = (1 << U2X0); /* this sets U2X0 to 1 */
	UCSR0B = (1 << RXEN0) | (1 << TXEN0) | (1 << RXCIE0);
	UCSR0C = (3 << UCSZ00); /* this sets UCSZ00 to 3 */

	/* baudrate setings (variable set by macros) */
	UBRR0H = (UBRRVAL) >> 8;
	UBRR0L = UBRRVAL;

	/* timer cfg */
	TCCR1A = (2 << COM1A0)| (2 << WGM10) ;						  /* CTC mode */
	TCCR1B = (3 << WGM12) | (1 << CS10);	/* internal /1 prescaler */
	ICR1 = OSC1A_VAL;
	OCR1A = OSC1A_VAL/2;
	TIMSK1 = (0 << OCIE1A);

	/* ADC cfg */
	sei();							  /* enable interrupts */
	uputs("hi!\n");

	clear_buffer();

	while (1) {		
		if (received) {
			received = 0;
			uputs("Echo: ");
			uputs(rx_buffer);
			uputs("\n");
			clear_buffer();
		}
	}
}
