## Getting Started with the TinkerCAD Simulator

1. Create an account an AutoDesk account to use [TinkerCAD](https://www.tinkercad.com/)
2. Go to the circuits tab in your TinkerCAD dashboard and create a new circuit
3. Search for the component *Arduino Uno R3* and drag it to the simulation environment
4. Select the *Code* tab on the top-right menu
5. In the drop-down menu replace *Blocks* with *Text*
6. Copy and paste the code in [blink_echo.c](https://gitlab.com/bernardocarvalho/dsPIC-codes/-/blob/master/Arduino/blink_echo.c) to the code editor in TinkerCAD
7. Click the *Start Simulation* button
8. Open the *Serial Monitor* in bottom-right corner
9. Type a string ending in "!" and hit return (the serial monitor does not transmit the \n and \r characters) 

The example code blinks and LED using a TIMER, and echoes every string ending in "!"

## Usefull links

#### TinkerCAD Simulator

* [Tutorial](https://www.programmingelectronics.com/arduino-simulator-tinkercad/)

#### Manuals

* [ATmega datasheet](http://ww1.microchip.com/downloads/en/DeviceDoc/ATmega48A-PA-88A-PA-168A-PA-328-P-DS-DS40002061A.pdf)