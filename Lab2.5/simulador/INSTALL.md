simavr ubuntu install instructions

0. install gcc-avr avrdude libelf-dev libncurses-dev libfreeglut3-dev avr-libc picocom
	apt install <gcc-avr... >
1. clone repository of simavr
	git clone https://github.com/buserror/simavr
2. change directory 
	cd simavr
3. compile simavr
	make
4. install simavr (need to be root)
	sudo make RELEASE=1 install
5. go back to home folder
	cd ~
6. clone laboratory example code 
	git clone https://gitlab.com/u-meft/dsPIC-codes
7. cd into the simulator folder
	cd dsPIC-codes/Lab2.5/simulator
8. compile the simulator
	make
9. run the simulator (take note of the pts number, refered to as xx from here on)
	./simduino
10. open a new tab on the terminal and cd into uart_example
	cd ../uart_example
11. program the simulator
	make PTS=xx up
12. open a new tab and open a serial terminal, default baud rate of example is 115200, you can also use putty or other software, exit with control+a control+x
	picocom -b <baudrate> /dev/pts/xx 

note: when you program the simulator the file main.c on the folder uart_example is compiled onto the simulator, replace the example with your own code after you confirm everything is working correctly

note2: you may need to restart the simulator to program it again if avrdude tells you that /dev/pts/xx is busy
