Arduino simulator that will open a virtual serial port on a pts
the simulator will run the boot loader, programming is still needed
use the makefile in the code subdirectory to accomplish this

remote workflow instructions:

1. clone this repo to obtain this folder structure, develop the code you wish
to run on the simulator and place the source file on the uart_example folder
2. Active the shell service on the dsi webpage https://selfservice.dsi.tecnico.ulisboa.pt/
3. ssh onto ist sigma service istxxxxxx@sigma.tecnico.ulisboa.pt
4. from the sigma ssh session, ssh onto the lab pc user@10.17.64.60
5. start screen (screen) (alternative, use tmux)
6. make a new folder for yourself, and clone you repo, with the your test code
and the simulation code.
7. compile the simulation code with make, and run it, take note of the virtual
terminal that is opened (i.e. /dev/pts/xx)
8. open a new window on screen (control+a c, control+a n to change into window
n, control+a k closes a window)
9. cd into uart_example
10. program the simulator with your code (warning, the simulator has a
timeout), your code must be in this folder named main.c, program with:
make PTS=xx up. where xx is the pts number you took note before.
11. open a serial terminal to test your code: screen /dev/pts/xx <baudrate>
	(alternative use picocom -b <baud> /dev/pts/xx, control+a control+x to exit)
