#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <stdio.h>

/* define baud rate as BAUD before this (can be defined externaly)*/

#define UBRRVAL F_CPU/8/BAUD-1

static int uputc(char,FILE*);

static int uputc(char c,FILE *stream)
{
	if (c == '\n')
		uputc('\r',stream);
	loop_until_bit_is_set(UCSR0A, UDRE0);	/* wait until we can send a new byte */
	UDR0 = (uint8_t) c;
	return 0;
}

static FILE mystdout = FDEV_SETUP_STREAM(uputc, NULL,_FDEV_SETUP_WRITE);

//uart receive isr
ISR(USART_RX_vect)
{
	uint8_t c = UDR0;
	putchar(c);
}

int main(void)
{
	stdout=&mystdout;

	/* pin config */
	DDRB = (1 << DDB5);

	/* uart config */
	UCSR0A = (1 << U2X0); /* this sets U2X0 to 1 */
	UCSR0B = (1 << RXEN0) | (1 << TXEN0) | (1 << RXCIE0);
	UCSR0C = (3 << UCSZ00); /* this sets UCSZ00 to 3 */

	/* baudrate setings (variable set by macros) */
	UBRR0H = (UBRRVAL) >> 8;
	UBRR0L = UBRRVAL;

	/* ADC cfg */
	sei();							  /* enable interrupts */
	puts("hi!");

	for (;;) {
		;/*nothing here, all is done on the uart interrupt*/
	}
}
